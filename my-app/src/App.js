import { Routes, Route } from "react-router-dom";
import Home from "./pages/Home";
import Login from "./pages/Login";
import Startpage from "./pages/Startpage";
import Logout from "./pages/Logout";
import PageNotFound from "./pages/PageNotFound";
import Browse from "./pages/Browse";
import SingleAdvertisement from "./pages/SingleAdvertisement";
import MySingleAdvertisement from "./pages/MySingleAdvertisement";
import NewAdvertisement from "./pages/NewAdvertisement";
import AdminHome from "./pages/AdminHome";



function App() {
  return (
    <>
        <Routes>
          <Route path="/" element={ <Startpage/> } />
          <Route path="/login" element={<Login/>} />
          <Route path="/home" element={<Home/>} />
          <Route path="/home/:addId" element={<MySingleAdvertisement/>} />
          <Route path="/adminHome" element={<AdminHome/>} />
          <Route path="/new" element={<NewAdvertisement/>} />
          <Route path="/logout" element={<Logout/>} />
          <Route path="*" element={<PageNotFound/>} />
          <Route path="/browse" element={<Browse/>} />
          <Route path="/browse/:addId" element={<SingleAdvertisement/>} />




        </Routes>
    </>
  );
}

export default App;
