import React from 'react'
import SignupForm from '../components/SignupForm';
import { NavLink, useNavigate } from 'react-router-dom'
import NavbarComponent from '../components/NavbarComponent';


function Startpage() {

 
  return (
    <main>        
        <section>
            <NavbarComponent/>
            <h1 style={{color:'#0080FF'}}> iTools </h1>
            <h4> Registrerings side </h4>
            {/* ønsker å sende funksjonen med herifra og til FormComponent, også i formcomponent så skriver jeg bare dynamisk inn en funksjon på onclick på button istedenfor å skrive en funksjon    */}
            <SignupForm/>    
            <p> Allerede medlem? {' '} 
                <NavLink to="/login" style={{color:'#0080FF'}}> Logg inn </NavLink>
            </p>
        
        </section>
    </main>
  )
}
 
export default Startpage