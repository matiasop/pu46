import React, { useState, useEffect } from 'react';
import { onAuthStateChanged } from "firebase/auth";
import { auth } from '../firebase/firebase';
import NavbarComponent from '../components/NavbarComponent';
import NotLoggedInHome from '../components/NotLoggedInHome';
import { getDatabase, ref, onValue} from "firebase/database";
import MyAdvertisementList from '../components/MyAdvertisementList';
import { Link } from 'react-router-dom';
import Button from 'react-bootstrap/Button';


function Home() {

  const [user, setUser] = useState([{}]);
  const [data, setData] = useState([{}]);
  const [userID, setUserID] = useState();
  

  useEffect(()=>{
    onAuthStateChanged(auth, (user) => {
        if (user) {
          setUserID(user.uid);
          //const uid = user.uid;
          const db = getDatabase();
          const userRef = ref(db, 'users/');
          onValue(userRef, (snapshot) => {
          const data = snapshot.val();
          console.log("FØR");
          console.log(data);
          console.log("ETTER");
          setData(data);
          setUser(user);
        });
        } else {
          // User is signed out
          console.log("user is logged out")
          console.log("user " + user);
          setUser(null)
        }
      });
     
}, [])

if (user) {
  console.log("LOGGED_IN_HOME_COMPONENT")
  console.log("USER " + user.email);
  return (
        <>
        <main >        
          <section>
            <div>
              <NavbarComponent/>                                            
              <h1 style={{color:'#0080FF'}}> Mine annonser </h1>                                          
              <div style={{ position: 'absolute', top: 80, right: 40 }}>
              <Link to={'/new'}>
                <Button variant="success"> Opprett ny annonse </Button>
              </Link>
              </div>
              
              <MyAdvertisementList userid={userID}/>
              
                           
            </div>
          </section>
        </main>
      </>   
  )
 }
 else {
   console.log("NOT_LOGGED_IN_HOME_COMPONENT")
   return (
     <>
     <NavbarComponent/>
       <NotLoggedInHome/>
     </>
   )
 }

}

export default Home