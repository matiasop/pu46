import React from 'react'
import { NavLink } from 'react-router-dom'
import LoginForm from '../components/LoginForm';
import NavbarComponent from '../components/NavbarComponent';

function Login() {
 
  return(
    <>
      <main >        
        <section>
          <div>
            <NavbarComponent/>                                            
            <h1 style={{color:'#0080FF'}}> iTools </h1>                       
            <h4> Logg inn side </h4>                       
            <LoginForm/>       
            <p> Ingen bruker enda? {' '} 
                <NavLink to="/" style={{color:'#0080FF'}}> Registrer deg </NavLink>
            </p>
                                         
          </div>
          
        </section>
      </main>
        </>
    )
}
 
export default Login