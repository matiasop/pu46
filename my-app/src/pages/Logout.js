import React from 'react';
import NavbarComponent from '../components/NavbarComponent';
import { NavLink } from 'react-router-dom'

function Logout() {
  return (
    <>
        <NavbarComponent/>
        <br></br>
        <h3 style={{color:'#0080FF'}}>Du er nå logget ut!</h3>
        <p> Ønsker å logge inn igjen? <NavLink to="/login" style={{color:'#0080FF'}}>Logg inn</NavLink> </p>
    </>
  )
}

export default Logout