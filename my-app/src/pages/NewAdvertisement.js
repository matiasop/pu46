import React from 'react';
import NewAddForm from '../components/NewAddForm';
import NavbarComponent from '../components/NavbarComponent';
import { useParams } from 'react-router-dom'


function NewAdvertisement() {

  const params = useParams();

  return (
    <>
      <main >        
        <section>
          <div>
            <NavbarComponent/>                                            
            <h1 style={{color:'blue'}}> Ny annonse </h1>                       
            <h4> Fyll ut alle feltene </h4>
            <NewAddForm/>                                                      
          </div>
        </section>
      </main>
    </>    
  );
}

export default NewAdvertisement;