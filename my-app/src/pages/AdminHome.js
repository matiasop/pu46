import React, { useEffect, useState } from 'react';
import { getAuth } from "firebase/auth";
import { getDatabase, ref, onValue, remove } from "firebase/database";
import { Link } from 'react-router-dom';


function AdminHome() {
  const [users, setUsers] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    // Fetch users from the database
    const fetchData = async () => {
      const db = getDatabase();
      const usersRef = ref(db, "users/");
      onValue(usersRef, (snapshot) => {
        const data = snapshot.val();
        setUsers(Object.values(data));
        setLoading(false);
      });
    }
    fetchData();
  }, []);

  // Logout function
  const handleLogout = () => {
    getAuth().signOut()
  }

  // Delete user function
  const handleDelete = (user) => {
    if (window.confirm(`Are you sure you want to delete ${user.name}?`)) {
      console.log("2")
      const userRef = ref(getDatabase(), `users/${user.entry}`);
      remove(userRef)
        .then(() => {
          setUsers(prevUsers => prevUsers.filter(u => u.entry !== user.entry));
        })
        .catch(error => {
          console.log("Error deleting user:", error.message);
          alert("Error deleting user. Please try again later.");
        });
      }
  
    }
    
  
  return (
    <div>
      <h1>Admin Dashboard</h1>
      {loading ? <h2>Loading...</h2> :
        <table>
          <thead>
            <tr>
              <th>Email</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            {users.map((user, key) => (
              <tr key={key}>
              <td>{user.email}</td>
              <td>
                <button onClick={() => handleDelete(user)}>Delete</button>
              </td>
            </tr>
            ))}
          </tbody>
        </table>
      }
      <Link to="/login">
      <button onClick={handleLogout}>Logout</button>
      </Link>
    </div>
  );
}

export default AdminHome;
