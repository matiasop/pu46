import React, { useState } from 'react';
import AdvertisementList from '../components/AdvertisementList';
import DataComponent from '../components/DataComponent';
import NavbarComponent from '../components/NavbarComponent';
import Form from 'react-bootstrap/Form';

function Browse() {

  const [searchWord, setSearchWord] = useState("");
  const [searchBool, setSearchBool] = useState(false);

  const handleInputChange = (event) => {
    setSearchWord(event.target.value);
    setSearchBool(true);
    if (event.target.value === "") {
      setSearchBool(false);
    }
  };

  
  return (
    <main>
      <section>
        <div>
          <NavbarComponent />
          <h1 style={{ color: "#0080FF" }}> Utstyr til leie </h1>

          <Form.Group className="mb-3" controlId="formBasicName">
            <Form.Label>Søk: </Form.Label>
            <Form.Control type="text" id="search" value={searchWord} onChange={handleInputChange} placeholder={"Skriv inn ønsket verktøy"} style={{borderRadius:5}} />
          </Form.Group>
          <br/>
          
          {searchBool && searchWord && <DataComponent searchWord={searchWord} />}
          {!searchBool && <AdvertisementList />}
        </div>
      </section>
    </main>
  );
}


export default Browse;