import React, { useState, useEffect }  from 'react'
import NavbarComponent from '../components/NavbarComponent';
import { useParams } from 'react-router-dom'
import '../components/App.css'
import { getDatabase, ref, onValue, update } from "firebase/database";
import { getAuth, onAuthStateChanged } from 'firebase/auth';
import Button from 'react-bootstrap/Button';
import { NavLink, useNavigate } from 'react-router-dom';

const SingleAdvertisement = () => {

  const navigate = useNavigate();
  const params = useParams();
  const [data, setData] = useState(null);
  const [currentUser, setCurrentUser] = useState(null);
  const auth = getAuth();

  useEffect(() => {
    const db = getDatabase();
    const userRef = ref(db, `advertisements/${params.addId}`);
    onValue(userRef, (snapshot) => {
      setData(snapshot.val());
    });

    onAuthStateChanged(auth, user => {
      setCurrentUser(user);
    });
  }, [params])

  const goBack = () => {
    navigate("/home")
  }
      
  const handleAvailabilityChange = () => {
    const db = getDatabase();
    const advertisementRef = ref(db, `advertisements/${params.addId}`);
    const newAvailabilityValue = data.availability ? 0 : 1;
    update(advertisementRef, { availability: newAvailabilityValue })
      .then(() => {
        setData({ ...data, availability: newAvailabilityValue });
      })
      .catch((error) => {
        console.error(error);
      });
  };

  return (
    <div>
      <NavbarComponent/>
      <br/>
      <button onClick={goBack}>Tibake</button>
      <br/><br/>
      {data && (
        <>
          <h3>{data.category}</h3>
          <h1 style={{color:'#0080FF'}}>{data.title}</h1>
          <p>{data.text}</p>
          <p> Pris for leie per dag: { data.price }  </p>

          <p> For avtale av leie, ta kontakt med:<br/> {data.email} </p>
          <img src={data.image} height={250}></img>
          <br/>
          <br/>
          <Button onClick={handleAvailabilityChange}>
            {data.availability ? "Gjør tilgjengelig" : "Gjør utilgjengelig"}
          </Button>
        </>
      )}
    </div>
  );
};

export default SingleAdvertisement;