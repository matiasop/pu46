import { useState } from "react";
import { ReactComponent as Sun } from "./sun.svg";
import { ReactComponent as Moon } from "./moon.svg";

import "./App.css";

const DarkMode = () => {
  const [isDark, setIsDark] = useState(
    document.body.getAttribute("data-theme") === "dark"
  );

  const setDarkMode = () => {
    document.body.setAttribute("data-theme", "dark");
    setIsDark(true);
  };

  const setLightMode = () => {
    document.body.setAttribute("data-theme", "light");
    setIsDark(false);
  };

  const handleToggleDarkMode = () => {
    const currentTheme = document.body.getAttribute("data-theme");
    if (currentTheme === "dark") {
      setLightMode();
    } else {
      setDarkMode();
    }
  };

  return (
    <div className="dark-mode">
      <button className="dark-mode-button" onClick={handleToggleDarkMode}>
        {isDark ? <Sun className="icon" /> : <Moon className="icon" />}
      </button>
    </div>
  );
};

export default DarkMode;




