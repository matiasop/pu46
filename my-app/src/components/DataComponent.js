import { getDatabase, ref, onValue} from "firebase/database";
import { useState, useEffect } from 'react';
import Card from 'react-bootstrap/Card';
import { Link } from 'react-router-dom';
import { getAuth, onAuthStateChanged } from 'firebase/auth';


function DataComponent(props) {
  const [data, setData] = useState(null);

  const [currentUser, setCurrentUser] = useState(null);
  const auth = getAuth();
  

  useEffect(()=>{
    const db = getDatabase();
    const userRef = ref(db, 'advertisements/');
    onValue(userRef, (snapshot) => {
      setData(snapshot.val());
    });

    onAuthStateChanged(auth, user => {
      setCurrentUser(user);
    });
  }, [])

    
  
  if (data && Object.entries(data).filter(([key, value]) => value.category.toLowerCase().startsWith(props.searchWord.toLowerCase())).length === 0) {
    return (
      <>
        <p> Ingen annonser matcher søket ditt </p>
      </>
    )
  }
  else {
    return (
      <>
        {data ? (
          <div className="advertisements">
            <div className="grid">
            {Object
            .entries(data)
            .filter(([key, value]) => value.category.toLowerCase().startsWith(props.searchWord.toLowerCase()))
            .map(([key, value]) => (
              <Card bg="dark" style={{ width: "18rem", border: "2px solid black" }} className="box" key={key}>
                <Card.Img variant="top" src={value.image}/>
                <Card.Body>
                  <Link to={`/browse/${key}`}>
                  <Card.Title>
                  { value.category }
                  </Card.Title>
                  <Card.Text>             
                    <h2 style={{color:'#0080FF'}}> { value.title } </h2>
                    <p> Pris: { value.price },- <br/> </p>    
                  </Card.Text> 
                  <img src={`${process.env.PUBLIC_URL}/${value.availability === 1 ? 'unavailable.png' : 'available.png'}`} alt="Availability" style={{ width: '50%' }} /> 
                  </Link>
                </Card.Body>
                
              </Card>
              
            ))}
            
            </div>
          </div>
          
        ) : (
          <p>Loading data...</p>
        )}
      </>
    );

  }
  
}

export default DataComponent;