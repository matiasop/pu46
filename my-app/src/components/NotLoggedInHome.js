import React from 'react';
import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

function NotLoggedInHome() {
  return (
    <>
      <br />

      <h1 style={{ color: '#0080FF', textAlign: 'center', marginTop: '50px'}}>iTools</h1>
      <p style={{ color: '#0080FF', textAlign: 'center' }}>Norges ledende leie og utleie nettside</p>

      <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', marginTop: '70px', marginBottom: '70px' }}>
 
  <Link to='/'>
    <Button variant="primary" style={{ marginRight: '10px' }}>Sign Up</Button>
  </Link>

  <Link to='/login'>
    <Button variant="secondary">Logg inn</Button>
  </Link>
</div>

      <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
        <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', margin: '0 20px' }}>
          <img src="bHammer.png" alt="Hammer" width="200" height="200" />
        </div>

        <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', margin: '0 170px' }}>
          <img src="bPensil.png" alt="Pencil" width="200" height="200" />
        </div>

        <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', margin: '0 20px' }}>
          <img src="bSkrewDriver.png" alt="Screwdriver" width="200" height="200" />
        </div>
      </div>

      
    </>
  );
}

export default NotLoggedInHome;