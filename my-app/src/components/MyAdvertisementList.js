import React, { useState, useEffect }  from 'react'
import Card from 'react-bootstrap/Card';
import { Link } from 'react-router-dom';
import '../components/App.css';
import { getDatabase, ref, onValue} from "firebase/database";
import { getAuth, onAuthStateChanged } from 'firebase/auth';



const MyAdvertisementList = () => {

  const [currentUser, setCurrentUser] = useState("");
  const auth = getAuth();

  const [data, setData] = useState("");

  useEffect(() => {

    const db = getDatabase();
    const userRef = ref(db, "advertisements/");
    onValue(userRef, (snapshot) => {
      setData(snapshot.val());
    });
    
    onAuthStateChanged(auth, user => {
      setCurrentUser(user);
    });
  
  }, [])

  if (data  === null) {
    return (
      <>
        <p> Account: {currentUser.email} </p>
        <p> Du har ingen annonser enda </p>
      </>
    )
  }
  
  if (Object.entries(data).filter(([key, value]) => value.userID === currentUser.uid).length === 0) {
    return (
      <>
        <p> Account: {currentUser.email} </p>
        <p> Du har ingen annonser enda </p>
      </>
    )
  }
  else {
    return (
      <>
        {data ? (
        <div className="advertisements">
          <p> Logget inn som: {currentUser.email}</p>
          <div className="grid">
            {Object
            .entries(data)
            .filter(([key, value]) => value.userID === currentUser.uid)
            .map(([key, value]) => (
              <ol key={key}>
                <Card bg="dark" style={{ width: "18rem", border: "2px solid black" }} className="box" key={key}>
                  <Card.Img variant="top" src={value.image} height={200}/>
                  <Card.Body>
                    <Link to={`/home/${key}`}>
                    <Card.Title>
                    { value.category }
                    </Card.Title>
                    <Card.Text>             
                      <h2 style={{color:'#0080FF'}}> { value.title } </h2>
                      <p> Pris: { value.price },- <br/> </p>    
                      <p> Min kontaktadresse: { value.email } </p>
                    </Card.Text> 
                    </Link>
                    <img src={`${process.env.PUBLIC_URL}/${value.availability === 1 ? 'unavailable.png' : 'available.png'}`} alt="Availability" style={{ width: '50%' }} /> 
                  </Card.Body>
                </Card>
                
              </ol>      
            ))}
            
            </div>
          </div>
          
        ) : (
          <p>Du har ingen egne annonser.</p>
        )}
      </>
    )
  }
  
}

export default MyAdvertisementList;
