import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import '../components/App.css';
import {useState} from 'react';
import { useNavigate } from 'react-router-dom';
import { auth } from '../firebase/firebase';
import {  createUserWithEmailAndPassword } from 'firebase/auth';
import { getDatabase, ref, set, push  } from "firebase/database";


function SignupForm() {
    // lager variabel for å slippe å kalle: useNavigate().navigate("/page")
    // kan nå kalle: navigate("/page") isteden
    const navigate = useNavigate();
 
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('');
    const [phonenumber, setPhonenumber] = useState('');
    const [name, setName] = useState('');
    const [errorMsg, setErrorMsg] = useState('');
      

    const [userEntryList, setUserEntryList] = useState([]);

  
    function writeUserData(userId, email, password, phonenumber, name) {
      const db = getDatabase();
      const postListRef = ref(db, 'users');
      const newPostRef = push(postListRef);
      set(newPostRef, {
        email: email,
        password: password,
        phonenumber: phonenumber,
        name: name,
        entry: newPostRef.key
      });      
    }

      
    
    
    const onSubmit = async (e) => {
      e.preventDefault();
      console.log("onSubmit function");
     
      await createUserWithEmailAndPassword(auth, email, password)
        .then((userCredential) => {
            // Signed in
            const user = userCredential.user;
            console.log(user);
            // sender brukeren til home page
            navigate("/home");
            // send denne brukeren + tlf til databasen
            writeUserData(user.uid, email, password, phonenumber, name);
        })

        // failed to sign up
        .catch((error) => {
            console.log("email: " + email + ", password: " + password);
            const errorCode = error.code;
            const errorMessage = error.message;
            console.log(errorCode, errorMessage);
            if (errorCode === "auth/weak-password") {
                setErrorMsg(errorMessage);
            }
            else {
                setErrorMsg(errorCode);
            }
        });
    }
    
  return (
    <>
      <Form style={{width:'30%'}}>
        <Form.Group className="mb-3" controlId="formBasicEmail">
          <Form.Label>Epost adresse</Form.Label>
          <Form.Control type="email" placeholder="Enter email" value={email} onChange={(e) => setEmail(e.target.value)} required/>
          <Form.Text className="text-muted">
          </Form.Text>
        </Form.Group>

        <Form.Group className="mb-3" controlId="formBasicPassword">
          <Form.Label> Passord </Form.Label>
          <Form.Control type="password" placeholder="Password" value={password} onChange={(e) => setPassword(e.target.value)} required/>
        </Form.Group>

        <Form.Group className="mb-3" controlId="formBasicPhonenumber">
          <Form.Label>Telefonnummer</Form.Label>
          <Form.Control type='number' placeholder="Phonenumber" value={phonenumber} onChange={(e) => setPhonenumber(e.target.value)} required/>
        </Form.Group>

        <Form.Group className="mb-3" controlId="formBasicName">
          <Form.Label>Navn</Form.Label>
          <Form.Control type='text' placeholder="Navn" value={name} onChange={(e) => setName(e.target.value)} required/>
        </Form.Group>


        <Button variant="primary" type="submit" onClick={onSubmit} >
          Send inn
        </Button>
      </Form>
      <div>
        <p style={{color:'red'}}>{errorMsg}</p>
      </div>
      </>
  );
}

export default SignupForm;