import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import 'bootstrap/dist/css/bootstrap.min.css';
import { NavLink } from 'react-router-dom'
import '../components/App.css';
import { auth } from '../firebase/firebase';
import DarkMode from './DarkMode';
import '../components/App.css';

function NavbarComponent() {

  function logout() {
    auth.signOut().then(function() {
      console.log('Signed Out');
      }, function(error) {
        console.error('Sign Out Error', error);
  }
  )}
  
  return (
    <>
      <Navbar bg= "secondary" variant= "dark">
        <Container>
          <Nav className='me-auto'>
            <NavLink to="/home" > Hjem </NavLink>
            <NavLink to="/login" > Logg inn </NavLink>
            <NavLink to="/" > Registrering </NavLink>
            <NavLink to="/browse"> Utforsk </NavLink>
            <DarkMode />
          </Nav>
          <Nav className="nav">
            <NavLink to="/home" onClick={logout}> Logg ut </NavLink>            
          </Nav>
        </Container>
      </Navbar>
    </>
  );
}

export default NavbarComponent;
