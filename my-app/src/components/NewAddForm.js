import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import '../components/App.css';
import {useState, useEffect} from 'react';
import { useNavigate } from 'react-router-dom';
import { getDatabase, ref, set, push  } from "firebase/database";
import { getAuth, onAuthStateChanged } from 'firebase/auth';
import { getStorage, ref as storageRef, uploadBytes, getDownloadURL } from "firebase/storage";



function NewAddForm() {
    // lager variabel for å slippe å kalle: useNavigate().navigate("/page")
    // kan nå kalle: navigate("/page") isteden
    const navigate = useNavigate();
 
    const [category, setCategory] = useState('')
    const [title, setTitle] = useState('')
    const [text, setText] = useState('');
    const [price, setPrice] = useState('');
    const [image, setImage] = useState(null);
    const [errorMsg, setErrorMsg] = useState('');

    const [currentUser, setCurrentUser] = useState(null);
    const auth = getAuth();


  useEffect(() => {
    onAuthStateChanged(auth, user => {
      setCurrentUser(user);
    });
  
  }, [])
  
    const onSubmit = async (e) => {
      e.preventDefault();
      if (!image) {
        setErrorMsg('Please select an image.');
        return;
      }
      // Upload image to storage
      const storage = getStorage();
      const imageRef = storageRef(storage, `images/${image.name}`);
      await uploadBytes(imageRef, image);

      // Save ad to database
      const db = getDatabase();
      const adRef = push(ref(db, 'advertisements/'));
      const adData = {
        image: await getDownloadURL(imageRef), // Get the download URL of the uploaded image
        title: title,
        category: category,
        text: text,
        userID: currentUser.uid,
        email: currentUser.email,
        price: price
      };
      set(adRef, adData);

      // Reset form
      setTitle("");
      setCategory("");
      setText("");
      setImage(null);

      navigate("/home");
    }


    
  return (
    <>
      <Form style={{width:'50%'}}>
        <Form.Group className="mb-3" controlId="formBasicCategory">
          <Form.Label> Kategori </Form.Label>
          <Form.Control placeholder="Navn på verktøy" value={category} onChange={(e) => setCategory(e.target.value)} required/>
          <Form.Text className="text-muted"> </Form.Text>
        </Form.Group>

        <Form.Group className="mb-3" controlId="formBasicTitle">
          <Form.Label> Beskrivende tekst </Form.Label>
          <Form.Control placeholder="Kort beskrivende informasjon" value={title} onChange={(e) => setTitle(e.target.value)} required/>
        </Form.Group>

        <Form.Group className="mb-3" controlId="formBasicText">
          <Form.Label> Utdypende informasjon </Form.Label>
          <Form.Control as="textarea" rows={3} placeholder="Informasjon om tilstanden til verktøyet, lånemuligheter etc." value={text} onChange={(e) => setText(e.target.value)} required/>
        </Form.Group>

        <Form.Group className="mb-3" controlId="formBasicPrice">
          <Form.Label> Pris for leie per dag </Form.Label>
          <Form.Control placeholder="Pris i NOK" value={price} onChange={(e) => setPrice(e.target.value)} required/>
        </Form.Group>

        <Form.Group className="mb-3" controlId="formFile" >
          <Form.Label> Last opp bilde av verktøyet </Form.Label>
          <Form.Control type="file" onChange={(e) => setImage(e.target.files[0])} required/>
        </Form.Group>

        <Button variant="primary" type="submit" onClick={onSubmit} >
          Send inn
        </Button>
      </Form>
      <div>
        <p style={{color:'red'}}>{errorMsg}</p>
      </div>
      </>
  );
}

export default NewAddForm;