import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import '../components/App.css';
import { useState } from 'react';
import { NavLink, useNavigate } from 'react-router-dom';
import { auth } from '../firebase/firebase';
import { signInWithEmailAndPassword } from 'firebase/auth';
import { getAuth, signInWithCustomToken } from 'firebase/auth';
import "firebase/auth";


function LoginForm() {
  // lager variabel for å slippe å kalle: useNavigate().navigate("/page")
  // kan nå kalle: navigate("/page") isteden
  const navigate = useNavigate();

  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('');

  const [errorMsg, setErrorMsg] = useState();

  const onLogin = (e) => {
    e.preventDefault();
    console.log("onLogin function")
  
    // Check if the email entered is the admin email
    if (email === "admin@gmail.com") {
      // Authenticate the user as an admin user
      signInWithEmailAndPassword(auth, email, password)
      .then((userCredential) => {
          // Signed in
          const user = userCredential.user;
          navigate("/AdminHome") // Redirect to the admin home page
          console.log(user);
      })
      // failed to login
      .catch((error) => {
          const errorCode = error.code;
          const errorMessage = error.message;
          console.log(errorCode, errorMessage)
          setErrorMsg(errorCode);
      });
    } else {
      // Authenticate the user as a regular user
      signInWithEmailAndPassword(auth, email, password)
      .then((userCredential) => {
          // Signed in
          const user = userCredential.user;
          navigate("/Home") // Redirect to the regular user home page
          console.log(user);
      })
      // failed to login
      .catch((error) => {
          const errorCode = error.code;
          const errorMessage = error.message;
          console.log(errorCode, errorMessage)
          setErrorMsg(errorCode);
      });
    }
  }
  
    
  return (
    <>
      <Form style={{width:'30%'}}>
        <Form.Group className="mb-3" controlId="formBasicEmail">
          <Form.Label>Epost addresse </Form.Label>
          <Form.Control type="email" placeholder="Enter email" value={email} onChange={(e) => setEmail(e.target.value)} required/>
          <Form.Text className="text-muted">
          </Form.Text>
        </Form.Group>

        <Form.Group className="mb-3" controlId="formBasicPassword">
          <Form.Label>Passord</Form.Label>
          <Form.Control type="password" placeholder="Password" value={password} onChange={(e) => setPassword(e.target.value)} required/>
        </Form.Group>

        <Button variant="primary" type="submit" onClick={onLogin} >
          Send inn
        </Button>
      </Form>
      <div>
        <p style={{color:'red'}}>{errorMsg}</p>
      </div>
      </>
  );
}

export default LoginForm;