import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getDatabase } from "firebase/database";
import { getStorage } from "firebase/storage";



const firebaseConfig = {
    apiKey: "AIzaSyBmGrG2GCDR_BWZyZ1tHuVe2RmUIgy-1gw",
    authDomain: "pu46-2b63c.firebaseapp.com",
    projectId: "pu46-2b63c",
    storageBucket: "pu46-2b63c.appspot.com",
    messagingSenderId: "645524066382",
    appId: "1:645524066382:web:1c04c1d97e07822916731f",
    measurementId: "G-WJMRWJPSJK",
    databaseURL: 'https://pu46-2b63c-default-rtdb.europe-west1.firebasedatabase.app'
  };



  const app = initializeApp(firebaseConfig);
  export const database = getDatabase(app);
  export const auth = getAuth(app);
  export const storage = getStorage(app);
  
  export default app;

