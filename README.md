## Tema
Denne applikasjonenen er en plattform hvor man kan låne verktøy og redskaper eller hobby utstyr som man sjeldent bruker og derfor ikke har selv. 

Applikasjonen retter seg spesielt mot unge uetablerte voksne, ettersom at de ofte har mangel på disse tingene i tillegg til å ha dårlig økonomi. 

## Hvordan kjøre applikasjonen:
    M1 mac
        1. cd my-app
        2. npm install
        3. npm start

    Annet
        1. cd my-app
        2. npm start



